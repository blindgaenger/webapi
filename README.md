webapi
======

Uses some of the finest http libs to give you a nice interface to work with the web.


## Notes

- https://github.com/mycozycloud/request-json/blob/master/main.coffee

## TODO

- defaults:
   headers:
     'Accepts': 'application/json'
- client takes default options in constructor
- file#upload
- can call request on module.exports
  JsonClient.get 'http://lala.com/foo.json'
