HttpClient = require './http'
cheerio = require 'cheerio'

class JsonClient extends HttpClient

  _parseResponse: (err, res, body, callback) ->
    json = if body? and res?.headers['content-type'].split(';')[0] == 'application/json'
      JSON.parse(body)
    else
      body
    super(err, res, json, callback)

module.exports = JsonClient
