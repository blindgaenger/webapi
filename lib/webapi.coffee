file = require './file'
http = require './http'
json = require './json'
html = require './html'

module.exports = {file, http, json, html}
