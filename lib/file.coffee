httpGet = require 'http-get'

module.exports =

  download: (url, file, callback) ->
    httpGet.get url, file, (err, result) ->
      if err?
        if err.code?
          callback(new Error("got #{err.code} on #{url}"))
        else
          callback(err)
        return

      callback()
