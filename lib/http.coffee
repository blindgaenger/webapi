request = require 'request'
deepmerge = require 'deepmerge'
{isPlainObject, partial} = require 'lodash'

class HttpClient
  @request: ->
    client = new HttpClient()
    client.request.apply(client, arguments)
    client

  defaults:
    headers:
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36'

  request: (method, url, headers, body, callback) ->
    [options, callback] = @_buildOptions(method, url, headers, body, callback)
    options = deepmerge(@defaults, options)
    request options, (err, res, body) =>
      @_parseResponse(err, res, body, callback)

  _buildOptions: (method, url, headers, body, callback) ->
    unless callback?
      unless body?
        callback = headers
        body = null
        headers = null

      else
        callback = body
        if isPlainObject(headers)
          body = null
          headers = headers
        else
          body = headers
          headers = null

    options =
      method: method
      uri: url
      headers: headers
      body: body

    return [options, callback]

  _parseResponse: (err, res, body, callback) ->
    return callback(err) if err?
    callback(err, res.statusCode, res.headers, body)

# curry some aliases
[
  'GET'
  'HEAD'
  'POST'
  'PUT'
  'PATCH'
  'DELETE'
].forEach (verb) ->
  HttpClient::[verb.toLowerCase()] = partial(HttpClient::request, verb)
  HttpClient[verb.toLowerCase()] = partial(HttpClient.request, verb)

module.exports = HttpClient
