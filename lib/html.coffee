HttpClient = require './http'
cheerio = require 'cheerio'

class HtmlClient extends HttpClient

  _isHtml: (res) ->
    res?.headers['content-type']?.match(/^text\/(html|xml)\b/)?

  _parseResponse: (err, res, body, callback) =>
    $ = if body? and @_isHtml(res)
      cheerio.load(body, {
        xmlMode: true
        lowerCaseTags: true
      })
    else
      body
    super(err, res, $, callback)

module.exports = HtmlClient
