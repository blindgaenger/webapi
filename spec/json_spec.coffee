{expect, fakeweb, sandbox} = require 'spec_helper'

JsonClient = require '../lib/json'
jsonClient = new JsonClient()

describe 'JsonClient', ->

  describe '#request', ->
    it 'parses json', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.json', {req: 'REQ'}, 'REQ_BODY',
          200, {'Content-Type': 'application/json; charset=utf-8'}, {body: 'BODY'}

        jsonClient.request 'GET', 'http://foo.com/index.json', {req: 'REQ'}, 'REQ_BODY', (err, code, headers, body) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'application/json; charset=utf-8'})
          expect(body).to.eql({body: 'BODY'})
          done(err)

    it 'leaves plain text', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.json', {req: 'REQ'}, 'REQ_BODY',
          200, {'Content-Type': 'text/plain'}, '<body>RES_BODY</body>'

        jsonClient.request 'GET', 'http://foo.com/index.json', {req: 'REQ'}, 'REQ_BODY', (err, code, headers, body) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'text/plain'})
          expect(body).to.eql('<body>RES_BODY</body>')
          done(err)

  describe 'aliases', ->
    it 'inherits the aliases', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.json', {}, null,
          200, {'Content-Type': 'application/json'}, {body: 'BODY'}

        jsonClient.request 'GET', 'http://foo.com/index.json', (err, code, headers, body) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'application/json'})
          expect(body).to.eql({body: 'BODY'})
          done(err)
