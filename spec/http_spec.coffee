{expect, fakeweb, sandbox} = require 'spec_helper'

HttpClient = require '../lib/http'
httpClient = new HttpClient()

describe 'HttpClient', ->

  describe '#defaults', ->
    it 'defines a user agent', ->
      expect(httpClient.defaults.headers['User-Agent']).to.exist

  describe '#request', ->
    it 'makes requests', (done) ->
      fakeweb (web) =>
        req =
          method: 'METHOD'
          url: 'http://foo.com/bar'
          headers: {foo: 'FOO'}
          body: 'REQ_BODY'

        res =
          code: 200
          headers: {bar: 'BAR'}
          body: 'RES_BODY'

        web.mock(
          req.method, req.url, req.headers, req.body,
          res.code, res.headers, res.body
        )

        httpClient.request req.method, req.url, req.headers, req.body, (err, code, headers, body) ->
          expect(code).to.eql(res.code)
          expect(headers).to.eql(res.headers)
          expect(body).to.eql(res.body)
          done(err)

    describe 'default arguments', ->
      beforeEach ->
        fakeweb (web) =>
          @method = 'METHOD'
          @url = 'http://foo.com/bar'
          @headers = {req: 'REQ'}
          @body = 'BODY'

          web.mock @method, @url, @headers, @body, 200, {}, 'METHOD,URL,HEADERS,BODY'
          web.mock @method, @url, @headers, null,  200, {}, 'METHOD,URL,HEADERS'
          web.mock @method, @url, {},       @body, 200, {}, 'METHOD,URL,BODY'
          web.mock @method, @url, {},       null,  200, {}, 'METHOD,URL'

          @assert_request = (result, args..., callback) =>
            httpClient.request @method, @url, args..., (err, code, headers, body) ->
              expect(body).to.eql(result)
              callback(err)

      afterEach ->
        fakeweb (web) =>
          web.reset()

      # none args
      it 'method, url', (done) -> @assert_request('METHOD,URL', done)

      # single arg
      it 'method, url, null',    (done) -> @assert_request('METHOD,URL', null, done)
      it 'method, url, headers', (done) -> @assert_request('METHOD,URL,HEADERS', @headers, done)
      it 'method, url, body',    (done) -> @assert_request('METHOD,URL,BODY', @body, done)

      # fill-in args
      it 'method, url, null, null',    (done) -> @assert_request('METHOD,URL', null, null, done)
      it 'method, url, headers, null', (done) -> @assert_request('METHOD,URL,HEADERS', @headers, null, done)
      it 'method, url, null, body',    (done) -> @assert_request('METHOD,URL,BODY', null, @body, done)

      # all args
      it 'method, url, headers, body', (done) -> @assert_request('METHOD,URL,HEADERS,BODY', @headers, @body, done)

    it 'follows redirects', (done) ->
      fakeweb (web) =>
        web.redirect 'http://foo.com/old', 'http://bar.com/new'
        web.get 'http://bar.com/new', 200, 'FOOBAR'

        httpClient.request 'GET', 'http://bar.com/new', {}, null, (err, code, headers, body) ->
          expect(body).to.eql('FOOBAR')
          done(err)

    it 'fails on http error', (done) ->
      httpClient.request 'GET', 'invalid.com', {}, null, (err, code, headers, body) ->
        expect(err).to.exist
        expect(body).be.not.exist
        done()

  describe 'aliases', ->
    [
      'GET'
      'HEAD'
      'POST'
      'PUT'
      'PATCH'
      'DELETE'
    ].forEach (verb) ->
      it "aliases #{verb}", (done) ->

        fakeweb (web) =>
          web.mock verb, "http://foo.com/#{verb}", {req: 'REQ'}, 'REQ_BODY', 200, {res: 'RES'}, 'RES_BODY'

          httpClient[verb.toLowerCase()] "http://foo.com/#{verb}", {req: 'REQ'}, 'REQ_BODY', (err, code, headers, body) ->
            expect(code).to.eql(200)
            expect(headers).to.eql({res: 'RES'})
            expect(body).to.eql('RES_BODY')
            done(err)
