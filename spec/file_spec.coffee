{expect, fakeweb} = require 'spec_helper'

fileModule = require '../lib/file'
fs = require 'fs'

describe 'file module', ->

  describe '#download', ->
    it 'gets a file from an url', (done) ->
      fakeweb (web) =>
        url = 'http://example.com/download/file.in'
        file = 'tmp/file.out'
        data = 'DATA'

        web.get url, 200, data
        fs.mkdirSync('tmp') unless fs.existsSync('tmp')

        # TODO: use proxyquire if possible
        fileModule.download url, file, (err) ->
          received = fs.readFileSync(file, 'utf8')
          expect(received).to.eql(data)
          done(err)

    it 'fails on error', (done) ->
      invalidUrl = ''
      file = 'tmp/dont.care'
      fileModule.download invalidUrl, file, (err, body) ->
        expect(err).to.exist
        expect(body).be.not.exist
        done()

    it 'fails on unsucessfull result', (done) ->
      fakeweb (web) =>
        url = 'http://foo.com/not/found'
        file = 'tmp/dont.care'

        web.get url, 404, 'ARGH'

        fileModule.download url, file, (err, body) ->
          expect(err).to.exist
          expect(err.message).to.match(/404/)
          expect(body).be.not.exist
          done()
