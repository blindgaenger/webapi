{expect} = require 'spec_helper'

webapi = require '../lib/webapi'

fileModule = require '../lib/file'
httpModule = require '../lib/http'
jsonModule = require '../lib/json'
htmlModule = require '../lib/html'

describe 'webapi', ->
  it 'exports all modules', ->
    expect(webapi.file).to.eql(fileModule)
    expect(webapi.http).to.eql(httpModule)
    expect(webapi.json).to.eql(jsonModule)
    expect(webapi.html).to.eql(htmlModule)
