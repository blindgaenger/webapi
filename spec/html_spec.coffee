{expect, fakeweb, sandbox} = require 'spec_helper'

HtmlClient = require '../lib/html'
htmlClient = new HtmlClient()

describe 'HtmlClient', ->

  describe '#request', ->
    it 'parses html', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.html', {req: 'REQ'}, 'REQ_BODY',
          200, {'Content-Type': 'text/html;ANYTHING'}, '<body>RES_BODY</body>'

        htmlClient.request 'GET', 'http://foo.com/index.html', {req: 'REQ'}, 'REQ_BODY', (err, code, headers, $) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'text/html;ANYTHING'})
          expect($.html()).to.eql('<body>RES_BODY</body>')
          done(err)

    it 'parses xml', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.xml', {req: 'REQ'}, 'REQ_BODY',
          200, {'Content-Type': 'text/xml;ANYTHING'}, '<xml>RES_XML</xml>'

        htmlClient.request 'GET', 'http://foo.com/index.xml', {req: 'REQ'}, 'REQ_BODY', (err, code, headers, $) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'text/xml;ANYTHING'})
          expect($.html()).to.eql('<xml>RES_XML</xml>')
          done(err)

    it 'leaves other formats', (done) ->
      fakeweb (web) =>
        web.mock 'GET',
          'http://foo.com/index.html', {req: 'REQ'}, 'REQ_BODY',
          200, {'Content-Type': 'CONTENT_TYPE'}, '<body>RES_BODY</body>'

        htmlClient.request 'GET', 'http://foo.com/index.html', {req: 'REQ'}, 'REQ_BODY', (err, code, headers, body) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'CONTENT_TYPE'})
          expect(body).to.eql('<body>RES_BODY</body>')
          done(err)

  describe 'aliases', ->
    it 'inherits the aliases', (done) ->
      fakeweb (web) =>
        web.mock 'GET', 'http://foo.com/index.html', {}, null, 200, {'Content-Type': 'text/html'}, '<body>RES_BODY</body>'

        htmlClient.request 'GET', 'http://foo.com/index.html', (err, code, headers, $) ->
          expect(code).to.eql(200)
          expect(headers).to.eql({'content-type': 'text/html'})
          expect($.html()).to.eql('<body>RES_BODY</body>')
          done(err)
