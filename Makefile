TEST_CMD = ./node_modules/.bin/mocha --compilers coffee:coffee-script spec/*_spec.coffee

test:
	$(TEST_CMD) --reporter min

test-watch:
	$(TEST_CMD) --reporter min --watch --growl

.PHONY: all
